module.exports = function(grunt) {

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        // Watch for new or updated JS / CSS files
        watch: {
            includeSource: {
                files: ['src/main/resources/static/app/**/*.js', 'src/main/resources/static/app/**/*.css'],
                tasks: ['includeSource'],
                options: {
                    event: ['added', 'deleted']
                }
            }
        },

        // Configuring Grunt Source
        includeSource: {
             options: {
                 basePath: 'src/main/resources/static/',
                 baseURL: ''
             },
            app: {
                files: {
                    'src/main/resources/static/index.html': 'src/main/resources/static/index.html'
                }
            }
         }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-include-source');

    grunt.registerTask('default', ['includeSource', 'watch']);
    grunt.registerTask('watcher', ['watch']);
}
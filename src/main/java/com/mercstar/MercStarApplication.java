package com.mercstar;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MercStarApplication {

	public static void main(String[] args) {
		SpringApplication.run(MercStarApplication.class, args);
	}
}

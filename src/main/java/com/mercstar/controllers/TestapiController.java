package com.mercstar.controllers;

import com.mercstar.api.Testapi;

import java.util.concurrent.atomic.AtomicLong;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TestapiController {

    private static final String template = "Hello, %s!";
    private final AtomicLong counter = new AtomicLong();

    @RequestMapping("/testapi")
    public Testapi testapi(@RequestParam(value = "name", defaultValue = "World") String name) {
        return new Testapi(counter.incrementAndGet(), String.format(template, name));
    }
}

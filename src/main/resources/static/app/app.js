'use strict';

var $stateProviderRef = null;

angular
    .module('mercstar', [
        'ui.router',
    ])
    .config(config)
    .run(initilize)
    .run(routes);

function config($stateProvider, $urlRouterProvider, $locationProvider) {

    $urlRouterProvider.deferIntercept();
    $urlRouterProvider.otherwise('/home');

    $locationProvider.html5Mode({enabled: false});
    $stateProviderRef = $stateProvider;
}

function initilize($rootScope, $state, $stateParams) {
    $rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
}

function routes($q, $rootScope, $http, $urlRouter) {
    $http
        .get("menu_routes.json")
        // Replace with API call so this is auto populated via DB
        .then(function(success)
        {
            $rootScope.menu_data = success.data;

            angular.forEach(success.data, function (value, key)
            {
                var state = {
                    "url": value.url,
                    "parent" : value.parent,
                    "abstract": value.abstract,
                    "templateUrl": value.templateUrl,
                    "controller": value.controller,
                    "controllerAs": value.controllerAs,
                };

                angular.forEach(value.views, function (view)
                {
                    state.views[view.name] = {
                        templateUrl : view.templateUrl,
                    };
                });

                $stateProviderRef.state(value.name, state);
            });
            // Configures $urlRouter's listener *after* your custom listener

            $urlRouter.sync();
            $urlRouter.listen();
        }
            , function(failed) {
                //Do Something Here #FIXME
        });
}


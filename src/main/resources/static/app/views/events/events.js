(function() {
    'use strict';

    angular
        .module('mercstar')
        .controller('EventController', EventController);

    EventController.$inject = ['$rootScope'];

    function EventController($rootScope) {
        var vm = this;

        vm.testString = "Event Test String";
        /** Code Here */
    }
})();